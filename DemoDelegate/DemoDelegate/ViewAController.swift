//
//  ViewController.swift
//  DemoDelegate
//
//  Created by LeAnhDuc on 18/04/2022.
//

import UIKit

class ViewAController: UIViewController, sentBackTaskDataProtocol {

    var listTask = ["ăn", "ngủ", ]
    
    @IBOutlet weak var listTaskTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }

    func onSent(data: String) {
        listTask.append(data)
        listTaskTableView.reloadData()
    }
    
    @IBAction func addTaskAction(_ sender: Any) {
        let viewBController = storyboard?.instantiateViewController(withIdentifier: "viewb") as! ViewBController
        viewBController.parentVC = self
        navigationController?.pushViewController(viewBController, animated: true)
    }
}

extension ViewAController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listTask.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TaskTableViewCell else {return UITableViewCell()}
        cell.nameLabel.text = listTask[indexPath.row]
        return cell
    }
    
    
}

