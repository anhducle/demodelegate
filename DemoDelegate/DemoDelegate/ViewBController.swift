//
//  ViewBController.swift
//  DemoDelegate
//
//  Created by LeAnhDuc on 18/04/2022.
//

import UIKit

protocol sentBackTaskDataProtocol{
    func onSent(data: String)
}

class ViewBController: UIViewController {

    var parentVC: ViewAController?
    @IBOutlet weak var taskTextField: UITextField!
    
    var delegate: sentBackTaskDataProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = parentVC
        
    }
    
    @IBAction func addTaskAction(_ sender: Any) {
        if taskTextField.text != ""{
            delegate?.onSent(data: taskTextField.text ?? "")
            navigationController?.popViewController(animated: true)
        }
    }
    
}




